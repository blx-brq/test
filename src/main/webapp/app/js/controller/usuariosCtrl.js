/**
 * 
 */

angular.module("app").controller("usuariosCtrl", function($rootScope, $location, $scope) {
	$scope.usuarios = [
	       			{usuario: "Leonardo Laia", telefone: "(41)00000000", email: "leonardo@teste.com", imagem: "https://image.freepik.com/free-icon/male-user-silhouette_318-35708.png", ativo: "s"},
	       			{usuario: "Felipe Denardo", telefone: "(41)11111111", email: "feliped@feliped.com", imagem: "https://image.freepik.com/free-icon/male-user-silhouette_318-35708.png", ativo: "n"},
	       			{usuario: "Iury Cunha", telefone: "(41)22222222", email: "iury@iury.com", imagem: "https://image.freepik.com/free-icon/male-user-silhouette_318-35708.png", ativo: "s"},
	       			{usuario: "Emerson Carpes", telefone: "(41)33333333", email: "emerson@emerson.com", imagem: "https://image.freepik.com/free-icon/male-user-silhouette_318-35708.png", ativo: "n"},
	       			{usuario: "Alexandre Hauffe", telefone: "(41)44444444", email: "alexandre@alexandre.com", imagem: "https://image.freepik.com/free-icon/male-user-silhouette_318-35708.png", ativo: "s"},
	       			{usuario: "Guilherme Tabalipa", telefone: "(41)55555555", email: "guilherme@guilherme.com", imagem: "https://image.freepik.com/free-icon/male-user-silhouette_318-35708.png", ativo: "s"},
	       			{usuario: "Matheus Mulder", telefone: "(41)6666666", email: "matheus@matheus.com", imagem: "https://image.freepik.com/free-icon/male-user-silhouette_318-35708.png", ativo: "s"},
	       			{usuario: "Felipe Pereira", telefone: "(41)77777777", email: "felipep@felipep.com", imagem: "https://image.freepik.com/free-icon/male-user-silhouette_318-35708.png", ativo: "n"},
	       			{usuario: "Bruno Mantovani", telefone: "(41)88888888", email: "bruno@bruno.com", imagem: "https://image.freepik.com/free-icon/male-user-silhouette_318-35708.png", ativo: "s"},
	       			{usuario: "Giovanni Beltrame", telefone: "(41)99999999", email: "giovanni@giovanni.com", imagem: "https://image.freepik.com/free-icon/male-user-silhouette_318-35708.png", ativo: "s"},
	       			{usuario: "Irineu Souza", telefone: "(41)12121212", email: "irineu@irineu.com", imagem: "https://image.freepik.com/free-icon/male-user-silhouette_318-35708.png", ativo: "n"}
	       			];
	       			$scope.desativar = function () {
	       				console.log($scope.usuario);
	       			};
	       			$scope.ativar = function () {
	       				console.log($scope.usuario);
	       			};
		});