/**
 * 
 */

angular.module("app").controller("listarAnuncioCtrl", function($rootScope, $location, $scope) {
	$scope.categorias = [
		     				{nome: 'Eletrônicos', subcategorias: [{nome: 'TV'}, {nome: 'PC'}, {nome: 'Notebooks'}] },
		     				{nome: 'Vestuário', subcategorias: [{nome: 'TV'}, {nome: 'PC'}, {nome: 'Notebooks'}]},
		     				{nome: 'Perfumaria', subcategorias: [{nome: 'TV'}, {nome: 'PC'}, {nome: 'Notebooks'}]},
		     				{nome: 'Móveis', subcategorias: [{nome: 'TV'}, {nome: 'PC'}, {nome: 'Notebooks'}]},
		     				{nome: 'Eletrodomésticos', subcategorias: [{nome: 'TV'}, {nome: 'PC'}, {nome: 'Notebooks'}]}
		     			];
	
	
	$scope.listaAnuncios = [
	                        {img: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg", descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.", 
	                        avaliacao: [{data: 'data', nota: 5},{data: 'data', nota: 4},{data: 'data', nota: 2}], nome: "Pedro", anunciante: "Luis", preco: 2900.00},

	                        {img: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg", descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.", 
	                        avaliacao: [{data: 'data', nota: 3},{data: 'data', nota: 4},{data: 'data', nota: 4}], nome: "Ana", anunciante: "Carlos", preco: 1800.00},

	                        {img: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg", descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.", 
	                        avaliacao: [{data: 'data', nota: 5},{data: 'data', nota: 4},{data: 'data', nota: 3}], nome: "João", anunciante: "Ana", preco: 1200.00},

	                        {img: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg", descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.", 
	                        avaliacao: [{data: 'data', nota: 5},{data: 'data', nota: 5},{data: 'data', nota: 5}], nome: "João", anunciante: "Ana", preco: 600.00},

	                        {img: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg", descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.", 
	                        avaliacao: [{data: 'data', nota: 5},{data: 'data', nota: 4},{data: 'data', nota: 5}], nome: "Maria", anunciante: "Luisa", preco: 400.00},

	                        {img: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg", descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.", 
	                        avaliacao: [{data: 'data', nota: 3},{data: 'data', nota: 4},{data: 'data', nota: 2}], nome: "João Paulo", anunciante: "Carlos", preco: 350.00},

	                        {img: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg", descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.", 
	                        avaliacao: [{data: 'data', nota: 5},{data: 'data', nota: 5},{data: 'data', nota: 3}], nome: "Henrique", anunciante: "Henrique", preco: 290.00},

	                        {img: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg", descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
	                        avaliacao: [{data: 'data', nota: 4},{data: 'data', nota: 4},{data: 'data', nota: 4}], nome: "João", anunciante: "João", preco: 1300.00},

	                        {img: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg", descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.", 
	                        avaliacao: [{data: 'data', nota: 5},{data: 'data', nota: 2},{data: 'data', nota: 3}], nome: "Ana Paula", anunciante: "Ana Paula", preco: 999.00}
	                        
	                        ];
	                      
	                      var CalcularMedia = function(listaAnuncios){
	                        var aux = listaAnuncios.avaliacao;
	                        var x = 0;
	                        var soma = 0;
	                        listaAnuncios.media = [];
	                        while(x < aux.length){          
	                          soma = soma + aux[x].nota;
	                          x++;          
	                        }
	                        var media = soma / aux.length;        
	                        listaAnuncios.media.push(media);
	                      }

	                      for(var x=0 ; x <$scope.listaAnuncios.length; x++){
	                        CalcularMedia($scope.listaAnuncios[x]);        
	                      }


	                        var carregarEstrelas = function(anuncio){      
	                          anuncio.estrelas = [];
	                          console.log(anuncio.media);
	                          var aux = anuncio.media;                  
	                            for(var x=0; x<5; x++){             
	                              if(aux >= 1){
	                                anuncio.estrelas.push("fa fa-star");                
	                              }else if(aux < 1 && aux > 0){
	                                anuncio.estrelas.push("fa fa-star-half-o");
	                              }else{
	                                anuncio.estrelas.push("fa fa-star-o");
	                              }
	                              aux--;                                            
	                            }                                 
	                        }

	                        for(var x=0; x < $scope.listaAnuncios.length; x++){
	                          carregarEstrelas($scope.listaAnuncios[x]);          
	                        }       

	                        console.log($scope.listaAnuncios);
	
		});