/**
 * 
 */

angular.module("app").controller("perfilCtrl", function($rootScope, $location, $scope) {
	$scope.contatos = {nome: "Leonardo Laia Ribeiro", foneFixo: "(41)3256-4545", foneMovel: "(41)9859-1245", email: "user@user.com"};

	$scope.negocios = [
		{prodNome: "Caixa velha", preco: 5, descricao: "Caixa velha, quero me livrar disso!", anunciante: {nome: 'Juca'}, imagem: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg", 
		avaliacao: [{data: 'data', nota: 1}, {data: 'data', nota: 1}, {data: 'data', nota: 1}]},

		{prodNome: "Monitor", preco: 1800, descricao: "monitor LED 42', 3 entradas HDMI, 4K", anunciante: {nome: 'Matias'}, imagem: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg",
		avaliacao: [{data: 'data', nota: 5}, {data: 'data', nota: 5}, {data: 'data', nota: 5}]},

		{prodNome: "Geladeira", preco: 700, descricao: "Geladeira Antiga, otimo estado de conservação, 220V",  anunciante: {nome: 'Gezabel'}, imagem: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg",
		avaliacao: [{data: 'data', nota: 2}, {data: 'data', nota: 3}, {data: 'data', nota: 3}]},

		{prodNome: "PS4", preco: 900, descricao: "Produto novo, acompanha caixa, 2 controles e 2 jogos",  anunciante: {nome: 'Carlos'}, imagem: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg",
		avaliacao: [{data: 'data', nota: 4}, {data: 'data', nota: 5}, {data: 'data', nota: 4}]}, 

		{prodNome: "Celular", preco: 400, descricao: "Celualar Nao sei que marca, sem riscos na tela, nunca caiu, trava um pouco ",  anunciante: {nome: 'Souza'}, imagem: "https://upload.wikimedia.org/wikipedia/en/5/5f/Original_Doge_meme.jpg",
		avaliacao: [{data: 'data', nota: 5}, {data: 'data', nota: 2.5}, {data: 'data', nota: 3.8}]}
	];

	var medias = []; 				  //Declara um vetor p/ armazenar as medias
	var CalcularMedia = function(negocios){   
		negocios.media = 0;    		  //insere a variavel media no obeto negocios 
		var aux = negocios.avaliacao; //recebo o avaliação do abjeto para nao altera-la
		var x = 0;
		var soma = 0;
		while(x < aux.length){  	  //Caulaculo da Media
		soma = soma + aux[x].nota;
		x++;          
		}
		soma = soma / aux.length;
		return soma;
		}

		for(var x=0 ; x <$scope.negocios.length; x++){ 		// Para cada negocio chamo a funcão de calcular a media da sua avaliação
			var soma = CalcularMedia($scope.negocios[x]);
			medias[x] = soma;        
		}
		console.log(medias);

		var carregarEstrelas = function(anuncio, media){  	  
			anuncio.estrelas = [];		//insere o vetor de estrelas no obeto negocios 	  				
			var aux = media;
			for(var x=0; x<5; x++){  	//logica das estrelas 
				if(aux >= 1){
					anuncio.estrelas.push("fa fa-star");
				}else if(aux < 1 && aux > 0){
					anuncio.estrelas.push("fa fa-star-half-o");
				}else{
					anuncio.estrelas.push("fa fa-star-o");
				}
				aux--;					 											  					
			}
			anuncio.media = media; 	  	  									  		
		}

		for(var x=0; x < $scope.negocios.length; x++){ 			//Para cada negocio faço a logica das estrelas 
			carregarEstrelas($scope.negocios[x], medias[x]);			
		}

		});