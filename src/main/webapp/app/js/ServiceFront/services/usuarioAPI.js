angular.module("app").factory(
		"usuarioAPI",
		function($http, config) {

			var _cadastrarUsuario = function(usuario) {
				return $http.post(
						config.baseUrl + "/services/api/usuario/cadastrar",
						usuario).then(handleSuccess,
						handleError('Erro ao cadastrar'));
			}

			var _excluirUsuario = function(idUsuario) {
				return $http.post(config.baseUrl + "/services/api/urlNetBeans",
						idUsuario);
			}

			var _editarUsuario = function(usuario) {
				return $http.post(config.baseUrl + "/services/api/urlNetBeans",
						usuario);
			}

			var _listarUsuario = function(nome) {
				return $http.get(config.baseUrl + "/services/api/urlNetBeans",
						nome);
			}

			var _ativarUsuario = function(idUsuario) {
				return $http.post(config.baseUrl + "/services/api/urlNetBeans",
						idUsuario);
			}

			var _logar = function(login, senha) {
				return $http.get(config.baseUrl + "/services/api/urlNetBeans",
						login, senha);
			}

			// funções de mensagem de erro da classe(privadas)
			function handleError(error) {
				return function() {
					return {
						success : false,
						message : error
					};
				};
			}
			function handleSuccess(res) {
				return res.data;
			}

			return {
				cadastrarUsuario : _cadastrarUsuario,
				excluirUsuario : _excluirUsuario,
				editarUsuario : _editarUsuario,
				listarUsuario : _listarUsuario,
				ativarUsuario : _ativarUsuario,
				logar : _logar
			}
		});