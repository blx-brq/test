angular.module("app").factory("anuncioAPI", function ($http, config) {

	var _listarAnuncios = function (parametrosCategoria, parametrosCriterioBusca) {
		return $http.get(config.baseUrl + "/services/api/urlNetBeans", parametrosCategoria, parametrosCriterioBusca);
	}

	var _cadastrarAnuncio = function(anuncio){
		return $http.post(config.baseUrl + "/services/api/anuncio/cadastrar", anuncio);
	}

	var _listarAnunciosUsuario = function(avaliacao, idUser){
		return $http.get(config.baseUrl + "/services/api/urlNetBeans", avaliacao, idUser);
	}

	var _cadastrarAvaliacao = function(valor){
		return $http.post(config.baseUrl + "/services/api/urlNetBeans", valor);
	}

	var _editarAnuncio = function(anuncio){
		return $htpp.post(config.baseUrl + "services/api/urlNetBeans", anuncio);
	}

	var _excluirAnuncio = function(idAnuncio){
		return $http.post(config.baseUrl + "services/api/urlNetBeans", idAnuncio);
	}

	var _exibirPerfilAnuncio = function(idAnuncio){
		return $http.get(config.baseUrl + "/services/api/urlNetBeans", idAnuncio);
	}

	var _fecharNegocio = function(idAnuncio){
		return $http.post(config.baseUrl + "/services/api/urlNetBeans", idAnuncio);
	}

	return{
		listarAnuncios: _listarAnuncios,
		cadastrarAnuncio: _cadastrarAnuncio,
		litarAnunciosUsuario: _listarAnunciosUsuario,
		cadastrarAvaliacao: _cadastrarAvaliacao,
		editarAnuncio: _editarAnuncio,
		excluirAnuncio: _excluirAnuncio,
		exibirPerfilAnuncio: _exibirPerfilAnuncio,
		fecharNegocio: _fecharNegocio
	}

});