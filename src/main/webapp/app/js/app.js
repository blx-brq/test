/**
 * tenho ciumes desse codigo mesmo u-u não faz merda nele 
 */

var app = angular.module('app', [ 'ui.router', 'ui.materialize',
		'angular.viacep', 'ngResource',
		'angularUtils.directives.dirPagination', 'ngCookies' ]);

app.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

	$urlRouterProvider.otherwise('/login');

	$stateProvider.state('login', {
		url : "/login",
		templateUrl : "partials/login.html",
		controller : "loginCtrl",
		data : {
			requireLogin : false
		}
	})

	.state('userCadastro', {
		url : "/userCadastro",
		templateUrl : "partials/userCadastro.html",
		controller : "userCadastroCtrl",
		data : {
			requireLogin : false
		}
	})

	.state('accountAdmin', {
		url : "/accountAdmin",
		templateUrl : "partials/homeAdmin.html",
		data : {
			requireLogin : true
		}
	})

	.state('accountUser', {
		url : "/accountUser",
		templateUrl : "partials/homeUser.html",
		data : {
			requireLogin : true
		}
	})

	.state('novoAnuncio', {
		url : "/novoAnuncio",
		templateUrl : "partials/novoAnuncio.html",
		controller : "novoAnuncioCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('alterarAnuncio', {
		url : "/alterarAnuncio",
		templateUrl : "partials/alterarAnuncio.html",
		data : {
			requireLogin : true
		}
	})

	.state('listarAnuncios', {
		url : "/anuncios",
		templateUrl : "partials/listarAnuncios.html",
		controller : "anuncioCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('listarAnunciosUsuario', {
		url : "/meus_anuncios",
		templateUrl : "partials/listarAnunciosUsuario.html",
		controller : "listarAnuncioUserCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('listarAnunciosAdmin', {
		url : "/anuncios_admin",
		templateUrl : "partials/listarAnunciosAdmin.html",
		controller : "listarAnuncioAdmCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('usuarios', {
		url : "/usuarios",
		templateUrl : "partials/usuarios.html",
		controller : "usuariosCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('perfil', {
		url : "/perfil",
		templateUrl : "partials/perfilUsuario.html",
		controller : "perfilCtrl",
		data : {
			requireLogin : true
		}
	})

});

app.run(function($rootScope, $location, $state, $cookies, $timeout) {
	$rootScope.$on('$stateChangeStart', function(event, toState, toParams,
			fromState, fromParams, options) {

		console.log($cookies);
		
	})
	// keep user logged in after page refresh

	$rootScope.globals = $cookies.get('globals') || {};
	if ($rootScope.globals.currentUser) {
		 $http.defaults.headers.common['Authorization'] = 'Basic ' +  //tipo de autorização.. as vezes está comentada para testes do via cep em novo anuncio 
		 $rootScope.globals.currentUser.authdata; // jshint ignore:line
	}

	$rootScope.$on('$locationChangeStart', function(event, next, current) {
		// redirect to login page if not logged in
		if ($location.path() !== '/login' && !$rootScope.globals.currentUser
				&& $location.path() !== '/userCadastro') {
			$location.path('/login');
			$location.path('/userCadastro');
		}

	});

});

// teste de autenticação de rota
// http://pt.stackoverflow.com/questions/111029/como-saber-qual-%C3%A9-o-location-atual
/*
 * $rootScope.$on('$stateChangeStart', function(event, toState, toParam,
 * fromState, fromParam) {
 * 
 * if(toState.data.requireLogin) { function validarAutenticacao() { //... };
 * 
 * Sua validação de autenticação Ela pode ser a mesma para todos os .state() que
 * possuirem requireLogin: true; Não precisa mais usar dentro do controller
 * 
 * if (validaAutenticacao == false) { //Se a autenticação for falsa
 * event.preventDefault(); //Impede que a página inicie o carregamento
 * //Redirecione o usuario ou faça outra manipulação aqui - como exibir um
 * alerta ou redirecionar para a página anterior. return $state.go('login'); } }
 * });
 * 
 * teste de validação com alert
 * 
 * if (toState.data.requireLogin == false) { // alert("aqui não precisa de
 * autenticação"); } else { // alert("aqui precisa de autenticação"); }
 *  // console.log(toState.name.indexOf('admin')) /* if
 * (toState.data.requireLogin) { function validarAutenticacao() { // ... } } ;
 * 
 * if (validaAutenticacao == false) { event.preventDefault(); // redireciona o
 * usuário caso de merda }
 * 
 */