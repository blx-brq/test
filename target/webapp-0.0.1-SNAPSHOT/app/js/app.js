/**
 * 
 */

var app = angular.module('app', [ 'ui.router', 'ui.materialize',
		'angular.viacep' ]);

app.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

	$urlRouterProvider.otherwise('/login');

	$stateProvider.state('login', {
		url : "/login",
		templateUrl : "partials/login.html",
		controller : "loginCtrl",
		data : {
			requireLogin : false
		}
	})

	.state('cadastro', {
		url : "/cadastro",
		templateUrl : "partials/cadastro.html",
		controller : "cadastroCtrl",
		data : {
			requireLogin : false
		}
	})

	.state('accountAdmin', {
		url : "/accountAdmin",
		templateUrl : "partials/homeAdmin.html",
		data : {
			requireLogin : true
		}
	})

	.state('accountUser', {
		url : "/accountUser",
		templateUrl : "partials/homeUser.html",
		data : {
			requireLogin : true
		}
	})

	.state('novoAnuncio', {
		url : "/novoAnuncio",
		templateUrl : "partials/novoAnuncio.html",
		controller: "novoAnuncioCtrl",
		data : {
			requireLogin : true
		}
	})

	.state('alterarAnuncio', {
		url : "/alterarAnuncio",
		templateUrl : "partials/alterarAnuncio.html",
		data : {
			requireLogin : true
		}
	})
	
	.state('detalheAnuncio', {
		url : "/detalheAnuncio",
		templateUrl : "partials/detalheAnuncio.html",
		controller: "detalheAnuncioCtrl",
		data : {
			requireLogin : true
		}
	})
});

app.run(function($rootScope, $location, $state) {
	$rootScope.$on('$stateChangeStart', function(event, toState, toParams,
			fromState, fromParams, options) {
		

		// console.log(toState);

		if (toState.data.requireLogin == false) {
			alert("aqui não precisa de autenticação");
		} else {
			alert("aqui precisa de autenticação");
		}

		// console.log(toState.name.indexOf('admin'))
		/*
		 * if (toState.data.requireLogin) { function validarAutenticacao() { //
		 * ... } } ;
		 * 
		 * if (validaAutenticacao == false) { event.preventDefault(); //
		 * redireciona o usuário caso de merda }
		 */

	})
});

// teste de autenticação de rota
// http://pt.stackoverflow.com/questions/111029/como-saber-qual-%C3%A9-o-location-atual
/*
 * $rootScope.$on('$stateChangeStart', function(event, toState, toParam,
 * fromState, fromParam) {
 * 
 * if(toState.data.requireLogin) { function validarAutenticacao() { //... };
 * 
 * Sua validação de autenticação Ela pode ser a mesma para todos os .state() que
 * possuirem requireLogin: true; Não precisa mais usar dentro do controller
 * 
 * if (validaAutenticacao == false) { //Se a autenticação for falsa
 * event.preventDefault(); //Impede que a página inicie o carregamento
 * //Redirecione o usuario ou faça outra manipulação aqui - como exibir um
 * alerta ou redirecionar para a página anterior. return $state.go('login'); } }
 * });
 */